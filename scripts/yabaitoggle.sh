
# changes workspace name in yabai
float_mode=$(yabai -m query --spaces | jq -r '.[] | select(.focused==1) | .type')


if [[ "$float_mode" = "bsp" ]]; then
	yabai -m space --layout float
	echo "now floating"
else
	yabai -m space --layout bsp
	echo "now bsping"
fi
