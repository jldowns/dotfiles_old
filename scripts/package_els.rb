require 'FileUtils'

source_dir = "/Users/justindowns/.emacs.d"
tmp_dir = "/tmp/packageel"
dest_zip = "./packaged.zip"
src_filetype = ".el"
dest_filetype = ".txt"

def replace_text_in_file(file, old_text, new_text)
  text = File.read(file)
  new_contents = text.gsub(old_text, new_text)
  File.open(file, "w") {|f| f.puts new_contents }
end


# Copy files, keeping directory structure
Dir.glob("**/*#{src_filetype}", base: source_dir).each{ |f|
  src = File.join(source_dir, f)
  new_name = File.basename(f, src_filetype) + dest_filetype
  dest = File.join(tmp_dir, File.dirname(f), new_name)

  FileUtils.mkdir_p File.dirname(dest)
  FileUtils.cp_r(src, dest)

  replace_text_in_file(dest, "justindowns", "userj")
}

# zip it
`zip -vr #{dest_zip} #{tmp_dir} -x "*.DS_Store"`

# remove tmp dir
FileUtils.remove_dir(tmp_dir)
