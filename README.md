# Symlink instructions


## Emacs
## skhd
## yabai
## spacebar

To start:

```
brew services start cmacrae/formulae/spacebar
```

## ubersicht
Widget folder:
/Users/justindowns/Library/Application Support/Übersicht/widgets

## oh-my-zsh

To install:
`sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`

## firefox
https://github.com/MiguelRAvila/SimplerentFox
Installed in:
(find-file "/Users/justindowns/Library/Application Support/Firefox/Profiles/wzrng5fo.default/chrome")



# Linux-specific
## i3-gaps
Installing:
```
sudo add-apt-repository ppa:kgilmer/speed-ricer
sudo apt-get update
```
