
# ** link spacemacs
# The .emacs.d directory is basically "off the shelf" from Spacemacs. I maintain
# my .spacemacs file and any layers I create in my private folder.
ln -s $(pwd)/spacemacs/.spacemacs $HOME/.spacemacs
ln -s $(pwd)/spacemacs/private $HOME/.emacs.d/private

# ** link skhd
mkdir -p $HOME/.config
ln -s $(pwd)/skhd $HOME/.config/skhd

# ** link yabai
# https://github.com/koekeishiya/yabai/wiki/Commands
mkdir -p $HOME/.config
ln -s $(pwd)/yabai $HOME/.config/yabai
ln -s $(pwd)/scripts/yabailabel.sh /usr/local/bin/yabailabel
ln -s $(pwd)/scripts/yabaitoggle.sh /usr/local/bin/yabaitoggle

# ** link ubersicht
ln -s $(pwd)/ubersicht $HOME/Library/Application\ Support/Übersicht/widgets/uberbar.widget

# ** link kitty
mkdir -p $HOME/.config
ln -s $(pwd)/kitty $HOME/.config/kitty

# ** link firefox
# note: your xxxx.default folder is probably different.
ln -s $(pwd)/firefox/chrome $HOME/Library/Application\ Support/Firefox/Profiles/wzrng5fo.default/chrome

# ** link ytop
mkdir -p $HOME/.config
ln -s $(pwd)/ytop $HOME/.config/ytop
