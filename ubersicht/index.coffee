

# * Command
command: "uberbar.widget/refresh_desktops.sh"

# * Refresh Frequequency
refreshFrequency: 1000

# * Style
style: """
  bottom: 0px
  left: 0px
  width: 99%
  height: 25px;

  padding-top 5px
  padding-left 24px
  padding-right 40px
  
  font-family: Menlo
  background-color: #f6f6f6
  font-size: 10pt
  color:red # hopefully we never see this

  span
    margin-right 40px
  span.focused
    color: #282a35
  span.unfocused
    color: #aaaaaa

  span#time_span
    float: right
    margin-left: 24px
    color: #282a35

  span#battery_span
    float: right
    margin-right: 24px
    color: #282a35

"""



# * Render
# Why do I put all the logic in the update section instead of the render section?
# Because if there is an error in the command (sometimes it returns a blank for some reason),
# I don't want to update the bar; just leave it as the previous values. `render` destroys all previous
# values. `update` allows me to keep them.
# So here I just place a span in the bar that `update` can use to build the bar.
render: (output) ->
    
    """
    <span id='bar'>
        initializing...
    <span>
    """

# * update
# Here's where I put all the logic. Read the `render` block for why.
update: (output, domEl) ->

# ** Parse output
# Try to parse the output from the refresh_desktop.sh script. Sometimes this script fails (todo :/)
# so if it does, just return and don't update the bar (keep the bar's value from the last render.)
    try
        parsed_output = JSON.parse(output)
    catch e
        return

# ** Create Time String
# Using javascript functions we create a string with the date and time. Then
# we add <span> tags and save the whole thing as a string.
    today = new Date
    day = today.getDay()
    day_of_month = today.getDate()
    daylist = [
      'sunday'
      'monday'
      'tuesday'
      'wednesday'
      'thursday'
      'friday'
      'saturday'
    ]
    hour = today.getHours()
    minute = today.getMinutes()
    second = today.getSeconds()
    time_string = "<span id='time_span'>#{daylist[day]}s... #{day_of_month} december #{hour}:#{minute}</span>"

# ** Create Desktop String
# We use the output from the refresh_desktop.sh script to render a workspace display.
# It's a series of spans with classes that define whether or not that workspace is currently
# selected.
    desktop_string = parsed_output["yab"].map (desktop) ->
        focused_class = if desktop.focused==1 then "focused" else "unfocused"
        desktop_number_string = if desktop.type == "bsp" then "(#{desktop.index})" else "#{desktop.index}:"
        """
        <span class="#{focused_class}">
            #{desktop_number_string}
            #{if desktop.label then desktop.label else "desktop "+desktop.index}
        </span>
        """
        
    desktop_string = desktop_string.join("\t")

    ## ** Create Battery String
    # Use the parsed script output to put some battery info up there.
    battery_string =
    """
    <span id="battery_span">
    #{if parsed_output["power_source"]=="AC Power" then "charging " else "battery"}
    #{parsed_output["power_percent"]}
    </span>
    """

# ** Write the bar's html
# Find the bar's span and write all the spans we were creating.
    $(domEl).find('#bar').html """

    <span>
        #{desktop_string}
        #{time_string}
        #{battery_string}
    <span>
    """
