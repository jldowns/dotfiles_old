#!/bin/bash

yabaioutput=$(/usr/local/bin/yabai -m query --spaces)
battery_report=$(pmset -g batt)

[[ "$battery_report" =~ \'(.*)\' ]]
powermode=${BASH_REMATCH[1]}
[[ "$battery_report" =~ [0-9]{2,3}\% ]]
powerpercent=${BASH_REMATCH}
[[ "$battery_report" =~ [0-9]{1,3}\:[0-9]{1,3} ]]
powertime=${BASH_REMATCH}


echo ' { "yab": '
echo $yabaioutput
echo ', "power_source": "'$powermode'"'
echo ', "power_percent": "'$powerpercent'"'
echo ', "power_time": "'$powertime'"'
echo '}'
