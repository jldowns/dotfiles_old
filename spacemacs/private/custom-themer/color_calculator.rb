require 'color_math'

s = 60
l = 50
number_of_colors = 8
hue_step = 360/number_of_colors

(0...360).step(hue_step).each { |h|
  cl = ColorMath.from_hsl(h, s, l)
  puts cl.to_hex
}
