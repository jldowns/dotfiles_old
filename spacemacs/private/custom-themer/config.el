;; Themeboy Configuration
;;
;; Themeboy based on work done by Dracula
;;
;; You will need:
;; - A background color
;; - 3 shades of gray/black
;; - 7 colors, including a good warning color
;;

;; My settings based on:
;; Colors are
;;    SAT:60,
;;    LITE:50,
;;    HUE:increments of 45 starting at 0
;; Background/hightlights are based on:
;;    https://www.colourlovers.com/palette/670816/i_n_v_i_s_i_b_l_e
;;
;; For theory on contrast ratios:
;; https://ux.stackexchange.com/questions/107318/formula-for-color-contrast-between-text-and-background

;; Background/forgrounds/highlightings
(setq  themeboy-bg1     "#fefffa") ; standard background color
(setq  themeboy-fg1     "#333333") ; standard text color
(setq  themeboy-current "#FCFEF5") ;; highlight subtle
(setq  themeboy-bg2     "#CDCFB7") ;; highlight less subtle
(set  'themeboy-fg2      themeboy-fg1) ; highlight less subtle text
(setq  themeboy-bg3     "#CDCFB7") ;; link hover
(set  'themeboy-fg3      themeboy-fg1) ; highlight less subtle text
(setq  themeboy-bg4     "#CDCFB7") ;; line numbers
(setq  themeboy-fg4     "#ff00ff") ;; tildes
;; Syntax Colors
(setq themeboy-comment  "#ADADAD")
(setq themeboy-error    "#CC3333") ;;
(setq themeboy-warning  "#CCA633")
(setq themeboy-string   "#80CC33") ; strings ;;
(setq themeboy-function "#7F33CC")  ; funtions
(setq themeboy-symbol   "#33CC59") ; "symbol" such as [1,2,3] or :key
(setq themeboy-class    "#33CCCC") ;;;
(setq themeboy-keyword  "#3359CC") ;;;
;; idk yet
(setq  other-blue       "#ff0000")
