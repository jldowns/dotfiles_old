;;; fizzy-theme.el --- Fizzy Theme

;; Copyright 2015-present, All rights reserved
;;
;; Code licensed under the MIT license

;; Author: film42
;; Version: 1.7.0
;; Package-Requires: ((emacs "24.3"))
;; URL: https://github.com/dracula/emacs

;;; Commentary:

;; A dark color theme available for a number of editors.

;;; Code:
(require 'cl-lib)
(deftheme fizzy)

;;;; Configuration options:

(defgroup fizzy nil
  "Fizzy theme options.

The theme has to be reloaded after changing anything in this group."
  :group 'faces)

(defcustom fizzy-enlarge-headings nil
  "Use different font sizes for some headings and titles."
  :type 'boolean
  :group 'fizzy)

(defcustom fizzy-height-title-1 1.3
  "Font size 100%."
  :type 'number
  :group 'fizzy)

(defcustom fizzy-height-title-2 1.1
  "Font size 110%."
  :type 'number
  :group 'fizzy)

(defcustom fizzy-height-title-3 1.0
  "Font size 130%."
  :type 'number
  :group 'fizzy)

(defcustom fizzy-height-doc-title 1.44
  "Font size 144%."
  :type 'number
  :group 'fizzy)

(defcustom fizzy-alternate-mode-line-and-minibuffer nil
  "Use less bold and pink in the minibuffer."
  :type 'boolean
  :group 'fizzy)

(defvar fizzy-use-24-bit-colors-on-256-colors-terms nil
  "Use true colors even on terminals announcing less capabilities.

Beware the use of this variable.  Using it may lead to unwanted
behavior, the most common one being an ugly blue background on
terminals, which don't understand 24 bit colors.  To avoid this
blue background, when using this variable, one can try to add the
following lines in their config file after having load the
Fizzy theme:

    (unless (display-graphic-p)
      (set-face-background 'default \"black\" nil))

There is a lot of discussion behind the 256 colors theme (see URL
`https://github.com/dracula/emacs/pull/57').  Please take time to
read it before opening a new issue about your will.")


(defvar  themeboy-bg1     "#fefffa") ; standard background color
(defvar  themeboy-fg1     "#333333") ; standard text color
(defvar  themeboy-bg2     "#CDCFB7") ;; highlight less subtle
(defvar  themeboy-fg2     "#333333") ; highlight less subtle text
(defvar  themeboy-bg3     "#CDCFB7") ;; link hover
(defvar  themeboy-fg3     "#333333") ; highlight less subtle text
(defvar  themeboy-bg4     "#0000ff") 
(defvar  themeboy-fg4     "#ff00ff") ;; tildes
(defvar  themeboy-comment "#ADADAD")
(defvar  themeboy-current "#FCFEF5") ;; highlight subtle
(defvar themeboy-error    "#CC3333") ;;
(defvar themeboy-warning  "#CCA633")
(defvar themeboy-string   "#80CC33") ; strings ;;
(defvar themeboy-function    "#7F33CC")  ; funtions
(defvar themeboy-symbol    "#33CC59") ;;
(defvar themeboy-class    "#33CCCC") ;;;
(defvar themeboy-keyword   "#3359CC") ;;;
(defvar  other-blue       "#ff0000")


;; Get the spaceline color to be editable
(setq spaceline-highlight-face-func 'spaceline-highlight-face-default)

;;;; Theme definition:

;; Assigment form: VARIABLE COLOR [256-COLOR [TTY-COLOR]]
(let ((colors `((fizzy-bg      ,themeboy-bg1)
                (fizzy-fg      ,themeboy-fg1)
                (fizzy-current ,themeboy-current)
                (fizzy-comment ,themeboy-comment)
                (fizzy-cyan    ,themeboy-symbol)
                (fizzy-green   ,themeboy-function)
                (fizzy-orange  ,themeboy-warning)
                (fizzy-pink    ,themeboy-keyword)
                (fizzy-purple  ,themeboy-class)
                (fizzy-red     ,themeboy-error)
                (fizzy-yellow  ,themeboy-string)
                ;; Other colors
                (bg2             ,themeboy-bg2)
                (bg3             ,themeboy-bg3)
                (bg4             ,themeboy-bg4)
                (fg2             ,themeboy-fg2)
                (fg3             ,themeboy-fg3)
                (fg4             ,themeboy-fg4)
                (other-blue      ,other-blue)))
      (faces '(;; default
               (cursor :background ,fg3)
               (completions-first-difference :foreground ,fizzy-pink :weight bold)
               (default :background ,fizzy-bg :foreground ,fizzy-fg)
               (default-italic :slant italic)
               (ffap :foreground ,fg4)
               (fringe :background ,fizzy-bg :foreground ,fg4)
               (highlight :foreground ,fg3 :background ,bg3)
               (hl-line :background ,fizzy-current :extend t)
               (info-quoted-name :foreground ,fizzy-orange)
               (info-string :foreground ,fizzy-yellow)
               (lazy-highlight :foreground ,fg2 :background ,bg2)
               (link :foreground ,fizzy-cyan :underline t)
               (linum :slant italic :foreground ,bg4 :background ,fizzy-bg)
               (line-number :slant italic :foreground ,bg4 :background ,fizzy-bg)
               (match :background ,fizzy-yellow :foreground ,fizzy-bg)
               (minibuffer-prompt
                ,@(if fizzy-alternate-mode-line-and-minibuffer
                      (list :weight 'normal :foreground fizzy-fg)
                    (list :weight 'bold :foreground fizzy-pink)))
               (read-multiple-choice-face :inherit completions-first-difference)
               (region :inherit match :extend t)
               (trailing-whitespace :foreground "unspecified-fg" :background ,fizzy-orange)
               (vertical-border :foreground ,bg2)
               (success :foreground ,fizzy-green)
               (warning :foreground ,fizzy-orange)
               (error :foreground ,fizzy-red)
               (header-line :background ,fizzy-bg)
               ;; syntax
               (font-lock-builtin-face :foreground ,fizzy-orange)
               (font-lock-comment-face :foreground ,fizzy-comment)
               (font-lock-comment-delimiter-face :foreground ,fizzy-comment)
               (font-lock-constant-face :foreground ,fizzy-cyan)
               (font-lock-doc-face :foreground ,fizzy-comment)
               (font-lock-function-name-face :foreground ,fizzy-green :weight bold)
               (font-lock-keyword-face :weight bold :foreground ,fizzy-pink)
               (font-lock-negation-char-face :foreground ,fizzy-cyan)
               (font-lock-preprocessor-face :foreground ,fizzy-orange)
               (font-lock-reference-face :foreground ,fizzy-cyan)
               (font-lock-regexp-grouping-backslash :foreground ,fizzy-cyan)
               (font-lock-regexp-grouping-construct :foreground ,fizzy-purple)
               (font-lock-string-face :foreground ,fizzy-yellow)
               (font-lock-type-face :foreground ,fizzy-purple)
               (font-lock-variable-name-face :foreground ,fizzy-fg
                                             :weight bold)
               (font-lock-warning-face :foreground ,fizzy-orange :background ,bg2)
               ;; auto-complete
               (ac-completion-face :underline t :foreground ,fizzy-pink)
               ;; company
               (company-echo-common :foreground ,fizzy-bg :background ,fizzy-fg)
               (company-preview :background ,fizzy-current :foreground ,other-blue)
               (company-preview-common :inherit company-preview
                                       :foreground ,fizzy-pink)
               (company-preview-search :inherit company-preview
                                       :foreground ,fizzy-green)
               (company-scrollbar-bg :background ,fizzy-comment)
               (company-scrollbar-fg :foreground ,other-blue)
               (company-tooltip :foreground ,fizzy-fg :background ,fizzy-current)
               (company-tooltip-search :foreground ,fizzy-green
                                       :underline t)
               (company-tooltip-search-selection :background ,fizzy-green
                                                 :foreground ,fizzy-bg)
               (company-tooltip-selection :inherit match)
               (company-tooltip-mouse :background ,fizzy-bg)
               (company-tooltip-common :foreground ,fizzy-pink :weight bold)
               ;;(company-tooltip-common-selection :inherit company-tooltip-common)
               (company-tooltip-annotation :foreground ,fizzy-cyan)
               ;;(company-tooltip-annotation-selection :inherit company-tooltip-annotation)
               ;; diff-hl
               (diff-hl-change :foreground ,fizzy-orange :background ,fizzy-orange)
               (diff-hl-delete :foreground ,fizzy-red :background ,fizzy-red)
               (diff-hl-insert :foreground ,fizzy-green :background ,fizzy-green)
               ;; dired
               (dired-directory :foreground ,fizzy-green :weight normal)
               (dired-flagged :foreground ,fizzy-pink)
               (dired-header :foreground ,fg3 :background ,fizzy-bg)
               (dired-ignored :inherit shadow)
               (dired-mark :foreground ,fizzy-fg :weight bold)
               (dired-marked :foreground ,fizzy-orange :weight bold)
               (dired-perm-write :foreground ,fg3 :underline t)
               (dired-symlink :foreground ,fizzy-yellow :weight normal :slant italic)
               (dired-warning :foreground ,fizzy-orange :underline t)
               (diredp-compressed-file-name :foreground ,fg3)
               (diredp-compressed-file-suffix :foreground ,fg4)
               (diredp-date-time :foreground ,fizzy-fg)
               (diredp-deletion-file-name :foreground ,fizzy-pink :background ,fizzy-current)
               (diredp-deletion :foreground ,fizzy-pink :weight bold)
               (diredp-dir-heading :foreground ,fg2 :background ,bg4)
               (diredp-dir-name :inherit dired-directory)
               (diredp-dir-priv :inherit dired-directory)
               (diredp-executable-tag :foreground ,fizzy-orange)
               (diredp-file-name :foreground ,fizzy-fg)
               (diredp-file-suffix :foreground ,fg4)
               (diredp-flag-mark-line :foreground ,fg2 :slant italic :background ,fizzy-current)
               (diredp-flag-mark :foreground ,fg2 :weight bold :background ,fizzy-current)
               (diredp-ignored-file-name :foreground ,fizzy-fg)
               (diredp-mode-line-flagged :foreground ,fizzy-orange)
               (diredp-mode-line-marked :foreground ,fizzy-orange)
               (diredp-no-priv :foreground ,fizzy-fg)
               (diredp-number :foreground ,fizzy-cyan)
               (diredp-other-priv :foreground ,fizzy-orange)
               (diredp-rare-priv :foreground ,fizzy-orange)
               (diredp-read-priv :foreground ,fizzy-purple)
               (diredp-write-priv :foreground ,fizzy-pink)
               (diredp-exec-priv :foreground ,fizzy-yellow)
               (diredp-symlink :foreground ,fizzy-orange)
               (diredp-link-priv :foreground ,fizzy-orange)
               (diredp-autofile-name :foreground ,fizzy-yellow)
               (diredp-tagged-autofile-name :foreground ,fizzy-yellow)
               ;; dired-subtree
               (dired-subtree-depth-1-face :background , bg2)
               (dired-subtree-depth-2-face :background , bg2)
               (dired-subtree-depth-3-face :background , bg2)
               (dired-subtree-depth-4-face :background , bg2)
               (dired-subtree-depth-5-face :background , bg2)
               (dired-subtree-depth-6-face :background , bg2)
               ;; elfeed
               (elfeed-search-date-face :foreground ,fizzy-comment)
               (elfeed-search-title-face :foreground ,fizzy-fg)
               (elfeed-search-unread-title-face :foreground ,fizzy-pink :weight bold)
               (elfeed-search-feed-face :foreground ,fizzy-fg :weight bold)
               (elfeed-search-tag-face :foreground ,fizzy-green)
               (elfeed-search-last-update-face :weight bold)
               (elfeed-search-unread-count-face :foreground ,fizzy-pink)
               (elfeed-search-filter-face :foreground ,fizzy-green :weight bold)
               ;;(elfeed-log-date-face :inherit font-lock-type-face)
               (elfeed-log-error-level-face :foreground ,fizzy-red)
               (elfeed-log-warn-level-face :foreground ,fizzy-orange)
               (elfeed-log-info-level-face :foreground ,fizzy-cyan)
               (elfeed-log-debug-level-face :foreground ,fizzy-comment)
               ;; elpher
               (elpher-gemini-heading1 :inherit bold :foreground ,fizzy-pink
                                       ,@(when fizzy-enlarge-headings
                                           (list :height fizzy-height-title-1)))
               (elpher-gemini-heading2 :inherit bold :foreground ,fizzy-purple
                                       ,@(when fizzy-enlarge-headings
                                           (list :height fizzy-height-title-2)))
               (elpher-gemini-heading3 :weight normal :foreground ,fizzy-green
                                       ,@(when fizzy-enlarge-headings
                                           (list :height fizzy-height-title-3)))
               (elpher-gemini-preformatted :inherit fixed-pitch
                                           :foreground ,fizzy-orange)
               ;; enh-ruby
               (enh-ruby-heredoc-delimiter-face :foreground ,fizzy-yellow)
               (enh-ruby-op-face :foreground ,fizzy-pink)
               (enh-ruby-regexp-delimiter-face :foreground ,fizzy-yellow)
               (enh-ruby-string-delimiter-face :foreground ,fizzy-yellow)
               ;; flyspell
               (flyspell-duplicate :underline (:style wave :color ,fizzy-orange))
               (flyspell-incorrect :underline (:style wave :color ,fizzy-red))
               ;; font-latex
               (font-latex-bold-face :foreground ,fizzy-purple)
               (font-latex-italic-face :foreground ,fizzy-pink :slant italic)
               (font-latex-match-reference-keywords :foreground ,fizzy-cyan)
               (font-latex-match-variable-keywords :foreground ,fizzy-fg)
               (font-latex-string-face :foreground ,fizzy-yellow)
               ;; gemini
               (gemini-heading-face-1 :inherit bold :foreground ,fizzy-pink
                                      ,@(when fizzy-enlarge-headings
                                          (list :height fizzy-height-title-1)))
               (gemini-heading-face-2 :inherit bold :foreground ,fizzy-purple
                                      ,@(when fizzy-enlarge-headings
                                          (list :height fizzy-height-title-2)))
               (gemini-heading-face-3 :weight normal :foreground ,fizzy-green
                                      ,@(when fizzy-enlarge-headings
                                          (list :height fizzy-height-title-3)))
               (gemini-heading-face-rest :weight normal :foreground ,fizzy-yellow)
               (gemini-quote-face :foreground ,fizzy-purple)
               ;; gnus-group
               (gnus-group-mail-1 :foreground ,fizzy-pink :weight bold)
               (gnus-group-mail-1-empty :inherit gnus-group-mail-1 :weight normal)
               (gnus-group-mail-2 :foreground ,fizzy-cyan :weight bold)
               (gnus-group-mail-2-empty :inherit gnus-group-mail-2 :weight normal)
               (gnus-group-mail-3 :foreground ,fizzy-comment :weight bold)
               (gnus-group-mail-3-empty :inherit gnus-group-mail-3 :weight normal)
               (gnus-group-mail-low :foreground ,fizzy-current :weight bold)
               (gnus-group-mail-low-empty :inherit gnus-group-mail-low :weight normal)
               (gnus-group-news-1 :foreground ,fizzy-pink :weight bold)
               (gnus-group-news-1-empty :inherit gnus-group-news-1 :weight normal)
               (gnus-group-news-2 :foreground ,fizzy-cyan :weight bold)
               (gnus-group-news-2-empty :inherit gnus-group-news-2 :weight normal)
               (gnus-group-news-3 :foreground ,fizzy-comment :weight bold)
               (gnus-group-news-3-empty :inherit gnus-group-news-3 :weight normal)
               (gnus-group-news-4 :inherit gnus-group-news-low)
               (gnus-group-news-4-empty :inherit gnus-group-news-low-empty)
               (gnus-group-news-5 :inherit gnus-group-news-low)
               (gnus-group-news-5-empty :inherit gnus-group-news-low-empty)
               (gnus-group-news-6 :inherit gnus-group-news-low)
               (gnus-group-news-6-empty :inherit gnus-group-news-low-empty)
               (gnus-group-news-low :foreground ,fizzy-current :weight bold)
               (gnus-group-news-low-empty :inherit gnus-group-news-low :weight normal)
               (gnus-header-content :foreground ,fizzy-pink)
               (gnus-header-from :foreground ,fizzy-fg)
               (gnus-header-name :foreground ,fizzy-purple)
               (gnus-header-subject :foreground ,fizzy-green :weight bold)
               (gnus-summary-markup-face :foreground ,fizzy-cyan)
               (gnus-summary-high-unread :foreground ,fizzy-pink :weight bold)
               (gnus-summary-high-read :inherit gnus-summary-high-unread :weight normal)
               (gnus-summary-high-ancient :inherit gnus-summary-high-read)
               (gnus-summary-high-ticked :inherit gnus-summary-high-read :underline t)
               (gnus-summary-normal-unread :foreground ,other-blue :weight bold)
               (gnus-summary-normal-read :foreground ,fizzy-comment :weight normal)
               (gnus-summary-normal-ancient :inherit gnus-summary-normal-read :weight light)
               (gnus-summary-normal-ticked :foreground ,fizzy-pink :weight bold)
               (gnus-summary-low-unread :foreground ,fizzy-comment :weight bold)
               (gnus-summary-low-read :inherit gnus-summary-low-unread :weight normal)
               (gnus-summary-low-ancient :inherit gnus-summary-low-read)
               (gnus-summary-low-ticked :inherit gnus-summary-low-read :underline t)
               (gnus-summary-selected :inverse-video t)
               ;; haskell-mode
               (haskell-operator-face :foreground ,fizzy-pink)
               (haskell-constructor-face :foreground ,fizzy-purple)
               ;; helm
               (helm-bookmark-w3m :foreground ,fizzy-purple)
               (helm-buffer-not-saved :foreground ,fizzy-purple :background ,fizzy-bg)
               (helm-buffer-process :foreground ,fizzy-orange :background ,fizzy-bg)
               (helm-buffer-saved-out :foreground ,fizzy-fg :background ,fizzy-bg)
               (helm-buffer-size :foreground ,fizzy-fg :background ,fizzy-bg)
               (helm-candidate-number :foreground ,fizzy-bg :background ,fizzy-fg)
               (helm-ff-directory :foreground ,fizzy-green :background ,fizzy-bg :weight bold)
               (helm-ff-dotted-directory :foreground ,fizzy-green :background ,fizzy-bg :weight normal)
               (helm-ff-executable :foreground ,other-blue :background ,fizzy-bg :weight normal)
               (helm-ff-file :foreground ,fizzy-fg :background ,fizzy-bg :weight normal)
               (helm-ff-invalid-symlink :foreground ,fizzy-pink :background ,fizzy-bg :weight bold)
               (helm-ff-prefix :foreground ,fizzy-bg :background ,fizzy-pink :weight normal)
               (helm-ff-symlink :foreground ,fizzy-pink :background ,fizzy-bg :weight bold)
               (helm-grep-cmd-line :foreground ,fizzy-fg :background ,fizzy-bg)
               (helm-grep-file :foreground ,fizzy-fg :background ,fizzy-bg)
               (helm-grep-finish :foreground ,fg2 :background ,fizzy-bg)
               (helm-grep-lineno :foreground ,fizzy-fg :background ,fizzy-bg)
               (helm-grep-match :foreground "unspecified-fg" :background "unspecified-bg" :inherit helm-match)
               (helm-grep-running :foreground ,fizzy-green :background ,fizzy-bg)
               (helm-header :foreground ,fg2 :background ,fizzy-bg :underline nil :box nil)
               (helm-moccur-buffer :foreground ,fizzy-green :background ,fizzy-bg)
               (helm-selection :background ,bg2 :underline nil)
               (helm-selection-line :background ,bg2)
               (helm-separator :foreground ,fizzy-purple :background ,fizzy-bg)
               (helm-source-go-package-godoc-description :foreground ,fizzy-yellow)
               (helm-source-header :foreground ,fizzy-pink :background ,fizzy-bg :underline nil :weight bold)
               (helm-time-zone-current :foreground ,fizzy-orange :background ,fizzy-bg)
               (helm-time-zone-home :foreground ,fizzy-purple :background ,fizzy-bg)
               (helm-visible-mark :foreground ,fizzy-bg :background ,bg3)
               ;; highlight-indentation minor mode
               (highlight-indentation-face :background ,bg2)
               ;; icicle
               (icicle-whitespace-highlight :background ,fizzy-fg)
               (icicle-special-candidate :foreground ,fg2)
               (icicle-extra-candidate :foreground ,fg2)
               (icicle-search-main-regexp-others :foreground ,fizzy-fg)
               (icicle-search-current-input :foreground ,fizzy-pink)
               (icicle-search-context-level-8 :foreground ,fizzy-orange)
               (icicle-search-context-level-7 :foreground ,fizzy-orange)
               (icicle-search-context-level-6 :foreground ,fizzy-orange)
               (icicle-search-context-level-5 :foreground ,fizzy-orange)
               (icicle-search-context-level-4 :foreground ,fizzy-orange)
               (icicle-search-context-level-3 :foreground ,fizzy-orange)
               (icicle-search-context-level-2 :foreground ,fizzy-orange)
               (icicle-search-context-level-1 :foreground ,fizzy-orange)
               (icicle-search-main-regexp-current :foreground ,fizzy-fg)
               (icicle-saved-candidate :foreground ,fizzy-fg)
               (icicle-proxy-candidate :foreground ,fizzy-fg)
               (icicle-mustmatch-completion :foreground ,fizzy-purple)
               (icicle-multi-command-completion :foreground ,fg2 :background ,bg2)
               (icicle-msg-emphasis :foreground ,fizzy-green)
               (icicle-mode-line-help :foreground ,fg4)
               (icicle-match-highlight-minibuffer :foreground ,fizzy-orange)
               (icicle-match-highlight-Completions :foreground ,fizzy-green)
               (icicle-key-complete-menu-local :foreground ,fizzy-fg)
               (icicle-key-complete-menu :foreground ,fizzy-fg)
               (icicle-input-completion-fail-lax :foreground ,fizzy-pink)
               (icicle-input-completion-fail :foreground ,fizzy-pink)
               (icicle-historical-candidate-other :foreground ,fizzy-fg)
               (icicle-historical-candidate :foreground ,fizzy-fg)
               (icicle-current-candidate-highlight :foreground ,fizzy-orange :background ,bg3)
               (icicle-Completions-instruction-2 :foreground ,fg4)
               (icicle-Completions-instruction-1 :foreground ,fg4)
               (icicle-completion :foreground ,fizzy-fg)
               (icicle-complete-input :foreground ,fizzy-orange)
               (icicle-common-match-highlight-Completions :foreground ,fizzy-purple)
               (icicle-candidate-part :foreground ,fizzy-fg)
               (icicle-annotation :foreground ,fg4)
               ;; icomplete
               (icompletep-determined :foreground ,fizzy-orange)
               ;; ido
               (ido-first-match
                ,@(if fizzy-alternate-mode-line-and-minibuffer
                      (list :weight 'normal :foreground fizzy-green)
                    (list :weight 'bold :foreground fizzy-pink)))
               (ido-only-match :foreground ,fizzy-orange)
               (ido-subdir :foreground ,fizzy-yellow)
               (ido-virtual :foreground ,fizzy-cyan)
               (ido-incomplete-regexp :inherit font-lock-warning-face)
               (ido-indicator :foreground ,fizzy-fg :background ,fizzy-pink)
               ;; isearch
               (isearch :inherit match :weight bold)
               (isearch-fail :foreground ,fizzy-bg :background ,fizzy-orange)
               ;; jde-java
               (jde-java-font-lock-constant-face :foreground ,fizzy-cyan)
               (jde-java-font-lock-modifier-face :foreground ,fizzy-pink)
               (jde-java-font-lock-number-face :foreground ,fizzy-fg)
               (jde-java-font-lock-package-face :foreground ,fizzy-fg)
               (jde-java-font-lock-private-face :foreground ,fizzy-pink)
               (jde-java-font-lock-public-face :foreground ,fizzy-pink)
               ;; js2-mode
               (js2-external-variable :foreground ,fizzy-purple)
               (js2-function-param :foreground ,fizzy-cyan)
               (js2-jsdoc-html-tag-delimiter :foreground ,fizzy-yellow)
               (js2-jsdoc-html-tag-name :foreground ,other-blue)
               (js2-jsdoc-value :foreground ,fizzy-yellow)
               (js2-private-function-call :foreground ,fizzy-cyan)
               (js2-private-member :foreground ,fg3)
               ;; js3-mode
               (js3-error-face :underline ,fizzy-orange)
               (js3-external-variable-face :foreground ,fizzy-fg)
               (js3-function-param-face :foreground ,fizzy-pink)
               (js3-instance-member-face :foreground ,fizzy-cyan)
               (js3-jsdoc-tag-face :foreground ,fizzy-pink)
               (js3-warning-face :underline ,fizzy-pink)
               ;; lsp
               (lsp-ui-peek-peek :background ,fizzy-bg)
               (lsp-ui-peek-list :background ,bg2)
               (lsp-ui-peek-filename :foreground ,fizzy-pink :weight bold)
               (lsp-ui-peek-line-number :foreground ,fizzy-fg)
               (lsp-ui-peek-highlight :inherit highlight :distant-foreground ,fizzy-bg)
               (lsp-ui-peek-header :background ,bg3 :foreground ,fg3, :weight bold)
               (lsp-ui-peek-footer :inherit lsp-ui-peek-header)
               (lsp-ui-peek-selection :inherit match)
               (lsp-ui-sideline-symbol :foreground ,fg4 :box (:line-width -1 :color ,fg4) :height 0.99)
               (lsp-ui-sideline-current-symbol :foreground ,fizzy-fg :weight ultra-bold
                                               :box (:line-width -1 :color fizzy-fg) :height 0.99)
               (lsp-ui-sideline-code-action :foreground ,fizzy-yellow)
               (lsp-ui-sideline-symbol-info :slant italic :height 0.99)
               (lsp-ui-doc-background :background ,fizzy-bg)
               (lsp-ui-doc-header :foreground ,fizzy-bg :background ,fizzy-cyan)
               ;; magit
               (magit-branch-local :foreground ,fizzy-cyan)
               (magit-branch-remote :foreground ,fizzy-green)
               (magit-tag :foreground ,fizzy-orange)
               (magit-section-heading :foreground ,fizzy-pink :weight bold)
               (magit-section-highlight :background ,bg3 :extend t)
               (magit-diff-context-highlight :background ,bg3
                                             :foreground ,fg3
                                             :extend t)
               (magit-diff-revision-summary :foreground ,fizzy-orange
                                            :background ,fizzy-bg
                                            :weight bold)
               (magit-diff-revision-summary-highlight :foreground ,fizzy-orange
                                                      :background ,bg3
                                                      :weight bold
                                                      :extend t)
               ;; the four following lines are just a patch of the
               ;; upstream color to add the extend keyword.
               (magit-diff-added :background "#335533"
                                 :foreground "#ddffdd"
                                 :extend t)
               (magit-diff-added-highlight :background "#336633"
                                           :foreground "#cceecc"
                                           :extend t)
               (magit-diff-removed :background "#553333"
                                   :foreground "#ffdddd"
                                   :extend t)
               (magit-diff-removed-highlight :background "#663333"
                                             :foreground "#eecccc"
                                             :extend t)
               (magit-diff-file-heading :foreground ,fizzy-fg)
               (magit-diff-file-heading-highlight :inherit magit-section-highlight)
               (magit-diffstat-added :foreground ,fizzy-green)
               (magit-diffstat-removed :foreground ,fizzy-red)
               (magit-hash :foreground ,fg2)
               (magit-hunk-heading :background ,bg3)
               (magit-hunk-heading-highlight :background ,bg3)
               (magit-item-highlight :background ,bg3)
               (magit-log-author :foreground ,fg3)
               (magit-process-ng :foreground ,fizzy-orange :weight bold)
               (magit-process-ok :foreground ,fizzy-green :weight bold)
               ;; markdown
               (markdown-blockquote-face :foreground ,fizzy-purple)
               (markdown-code-face :foreground ,fizzy-orange)
               (markdown-footnote-face :foreground ,other-blue)
               (markdown-header-face :weight normal)
               (markdown-header-face-1
                :inherit bold :foreground ,fizzy-pink
                ,@(when fizzy-enlarge-headings
                    (list :height fizzy-height-title-1)))
               (markdown-header-face-2
                :inherit bold :foreground ,fizzy-purple
                ,@(when fizzy-enlarge-headings
                    (list :height fizzy-height-title-2)))
               (markdown-header-face-3
                :foreground ,fizzy-green
                ,@(when fizzy-enlarge-headings
                    (list :height fizzy-height-title-3)))
               (markdown-header-face-4 :foreground ,fizzy-yellow)
               (markdown-header-face-5 :foreground ,fizzy-cyan)
               (markdown-header-face-6 :foreground ,fizzy-orange)
               (markdown-header-face-7 :foreground ,other-blue)
               (markdown-header-face-8 :foreground ,fizzy-fg)
               (markdown-inline-code-face :foreground ,fizzy-yellow)
               (markdown-plain-url-face :inherit link)
               (markdown-pre-face :foreground ,fizzy-orange)
               (markdown-table-face :foreground ,fizzy-purple)
               ;; message
               (message-header-to :foreground ,fizzy-fg :weight bold)
               (message-header-cc :foreground ,fizzy-fg :bold bold)
               (message-header-subject :foreground ,fizzy-orange)
               (message-header-newsgroups :foreground ,fizzy-purple)
               (message-header-other :foreground ,fizzy-purple)
               (message-header-name :foreground ,fizzy-green)
               (message-header-xheader :foreground ,fizzy-cyan)
               (message-separator :foreground ,fizzy-cyan :slant italic)
               (message-cited-text :foreground ,fizzy-purple)
               (message-cited-text-1 :foreground ,fizzy-purple)
               (message-cited-text-2 :foreground ,fizzy-orange)
               (message-cited-text-3 :foreground ,fizzy-comment)
               (message-cited-text-4 :foreground ,fg2)
               (message-mml :foreground ,fizzy-green :weight normal)
               ;; mode-line
               (mode-line :background ,fizzy-current
                          :box nil :inverse-video nil
                          ,@(if fizzy-alternate-mode-line-and-minibuffer
                                (list :foreground fg3)
                              (list :foreground "unspecified-fg")))
               (mode-line-inactive
                :inverse-video nil
                ,@(if fizzy-alternate-mode-line-and-minibuffer
                      (list :foreground fizzy-comment :background fizzy-bg
                            :box fizzy-bg)
                    (list :foreground fizzy-fg :background bg2 :box bg2)))
               ;; mu4e
               (mu4e-unread-face :foreground ,fizzy-pink :weight normal)
               (mu4e-view-url-number-face :foreground ,fizzy-purple)
               (mu4e-highlight-face :background ,fizzy-bg
                                    :foreground ,fizzy-yellow
                                    :extend t)
               (mu4e-header-highlight-face :background ,fizzy-current
                                           :foreground ,fizzy-fg
                                           :underline nil :weight bold
                                           :extend t)
               (mu4e-header-key-face :inherit message-mml)
               (mu4e-header-marks-face :foreground ,fizzy-purple)
               (mu4e-cited-1-face :foreground ,fizzy-purple)
               (mu4e-cited-2-face :foreground ,fizzy-orange)
               (mu4e-cited-3-face :foreground ,fizzy-comment)
               (mu4e-cited-4-face :foreground ,fg2)
               (mu4e-cited-5-face :foreground ,fg3)
               ;; neotree
               (neo-banner-face :foreground ,fizzy-orange :weight bold)
               ;;(neo-button-face :underline nil)
               (neo-dir-link-face :foreground ,fizzy-purple)
               (neo-expand-btn-face :foreground ,fizzy-fg)
               (neo-file-link-face :foreground ,fizzy-cyan)
               (neo-header-face :background ,fizzy-bg
                                :foreground ,fizzy-fg
                                :weight bold)
               (neo-root-dir-face :foreground ,fizzy-purple :weight bold)
               (neo-vc-added-face :foreground ,fizzy-orange)
               (neo-vc-conflict-face :foreground ,fizzy-red)
               (neo-vc-default-face :inherit neo-file-link-face)
               (neo-vc-edited-face :foreground ,fizzy-orange)
               (neo-vc-ignored-face :foreground ,fizzy-comment)
               (neo-vc-missing-face :foreground ,fizzy-red)
               (neo-vc-needs-merge-face :foreground ,fizzy-red
                                        :weight bold)
               ;;(neo-vc-needs-update-face :underline t)
               ;;(neo-vc-removed-face :strike-through t)
               (neo-vc-unlocked-changes-face :foreground ,fizzy-red)
               ;;(neo-vc-unregistered-face nil)
               (neo-vc-up-to-date-face :foreground ,fizzy-green)
               (neo-vc-user-face :foreground ,fizzy-purple)
               ;; org
               (org-agenda-date :foreground ,fizzy-cyan :underline nil)
               (org-agenda-dimmed-todo-face :foreground ,fizzy-comment)
               (org-agenda-done :foreground ,fizzy-green)
               (org-agenda-structure :foreground ,fizzy-purple)
               (org-block :foreground ,fizzy-orange)
               (org-code :foreground ,fizzy-yellow)
               (org-column :background ,bg4)
               (org-column-title :inherit org-column :weight bold :underline t)
               (org-date :foreground ,fizzy-cyan :underline t)
               (org-document-info :foreground ,other-blue)
               (org-document-info-keyword :foreground ,fizzy-comment)
               (org-document-title :weight bold :foreground ,fizzy-orange
                                   ,@(when fizzy-enlarge-headings
                                       (list :height fizzy-height-doc-title)))
               (org-done :foreground ,fizzy-green)
               (org-ellipsis :foreground ,fizzy-comment)
               (org-footnote :foreground ,other-blue)
               (org-formula :foreground ,fizzy-pink)
               (org-headline-done :foreground ,fizzy-comment
                                  :weight normal :strike-through t)
               (org-hide :foreground ,fizzy-bg :background ,fizzy-bg)
               (org-level-1 :inherit bold :foreground ,fizzy-pink
                            ,@(when fizzy-enlarge-headings
                                (list :height fizzy-height-title-1)))
               (org-level-2 :inherit bold :foreground ,fizzy-purple
                            ,@(when fizzy-enlarge-headings
                                (list :height fizzy-height-title-2)))
               (org-level-3 :weight normal :foreground ,fizzy-green
                            ,@(when fizzy-enlarge-headings
                                (list :height fizzy-height-title-3)))
               (org-level-4 :weight normal :foreground ,fizzy-yellow)
               (org-level-5 :weight normal :foreground ,fizzy-cyan)
               (org-level-6 :weight normal :foreground ,fizzy-orange)
               (org-level-7 :weight normal :foreground ,other-blue)
               (org-level-8 :weight normal :foreground ,fizzy-fg)
               (org-link :foreground ,fizzy-cyan :underline t)
               (org-priority :foreground ,fizzy-cyan)
               (org-scheduled :foreground ,fizzy-green)
               (org-scheduled-previously :foreground ,fizzy-yellow)
               (org-scheduled-today :foreground ,fizzy-green)
               (org-sexp-date :foreground ,fg4)
               (org-special-keyword :foreground ,fizzy-yellow)
               (org-table :foreground ,fizzy-purple)
               (org-tag :foreground ,fizzy-pink :weight bold :background ,bg2)
               (org-todo :foreground ,fizzy-orange :weight bold :background ,bg2)
               (org-upcoming-deadline :foreground ,fizzy-yellow)
               (org-warning :weight bold :foreground ,fizzy-pink)
               ;; outline
               (outline-1 :foreground ,fizzy-pink)
               (outline-2 :foreground ,fizzy-purple)
               (outline-3 :foreground ,fizzy-green)
               (outline-4 :foreground ,fizzy-yellow)
               (outline-5 :foreground ,fizzy-cyan)
               (outline-6 :foreground ,fizzy-orange)
               ;; spaceline
               (spaceline-evil-base :foreground ,bg2)
               (spaceline-evil-emacs :inherit spaceline-evil-base :background ,fizzy-yellow)
               (spaceline-evil-insert :inherit spaceline-evil-base :background ,fizzy-cyan)
               (spaceline-evil-motion :inherit spaceline-evil-base :background ,fizzy-purple)
               (spaceline-evil-normal :inherit spaceline-evil-base :background ,fizzy-green)
               (spaceline-evil-operator :inherit spaceline-evil-base :background ,fizzy-pink)
               (spaceline-evil-replace :inherit spaceline-evil-base :background ,fizzy-red)
               (spaceline-evil-visual :inherit spaceline-evil-base :background ,fizzy-orange)
               (spaceline-highlight-face :inherit spaceline-evil-base)
               ;; powerline
               (powerline-active1 :background ,fizzy-bg :foreground ,fizzy-pink)
               (powerline-active2 :background ,fizzy-bg :foreground ,fizzy-pink)
               (powerline-inactive1 :background ,bg2 :foreground ,fizzy-purple)
               (powerline-inactive2 :background ,bg2 :foreground ,fizzy-purple)
               (powerline-evil-base-face :foreground ,bg2)
               (powerline-evil-emacs-face :inherit powerline-evil-base-face :background ,fizzy-yellow)
               (powerline-evil-insert-face :inherit powerline-evil-base-face :background ,fizzy-cyan)
               (powerline-evil-motion-face :inherit powerline-evil-base-face :background ,fizzy-purple)
               (powerline-evil-normal-face :inherit powerline-evil-base-face :background ,fizzy-green)
               (powerline-evil-operator-face :inherit powerline-evil-base-face :background ,fizzy-pink)
               (powerline-evil-replace-face :inherit powerline-evil-base-face :background ,fizzy-red)
               (powerline-evil-visual-face :inherit powerline-evil-base-face :background ,fizzy-orange)
               ;; rainbow-delimiters
               (rainbow-delimiters-depth-1-face :foreground ,fizzy-fg)
               (rainbow-delimiters-depth-2-face :foreground ,fizzy-cyan)
               (rainbow-delimiters-depth-3-face :foreground ,fizzy-purple)
               (rainbow-delimiters-depth-4-face :foreground ,fizzy-pink)
               (rainbow-delimiters-depth-5-face :foreground ,fizzy-orange)
               (rainbow-delimiters-depth-6-face :foreground ,fizzy-green)
               (rainbow-delimiters-depth-7-face :foreground ,fizzy-yellow)
               (rainbow-delimiters-depth-8-face :foreground ,other-blue)
               (rainbow-delimiters-unmatched-face :foreground ,fizzy-orange)
               ;; rpm-spec
               (rpm-spec-dir-face :foreground ,fizzy-green)
               (rpm-spec-doc-face :foreground ,fizzy-pink)
               (rpm-spec-ghost-face :foreground ,fizzy-purple)
               (rpm-spec-macro-face :foreground ,fizzy-yellow)
               (rpm-spec-obsolete-tag-face :inherit font-lock-warning-face)
               (rpm-spec-package-face :foreground ,fizzy-purple)
               (rpm-spec-section-face :foreground ,fizzy-yellow)
               (rpm-spec-tag-face :foreground ,fizzy-cyan)
               (rpm-spec-var-face :foreground ,fizzy-orange)
               ;; selectrum-mode
               (selectrum-current-candidate :foreground ,fizzy-pink)
               (selectrum-primary-highlight :foreground ,fizzy-orange)
               (selectrum-secondary-highlight :foreground ,fizzy-green)
               ;; show-paren
               (show-paren-match-face :background unspecified
                                      :foreground ,fizzy-cyan
                                      :weight bold)
               (show-paren-match :background unspecified
                                 :foreground ,fizzy-cyan
                                 :weight bold)
               (show-paren-match-expression :inherit match)
               (show-paren-mismatch :inherit font-lock-warning-face)
               ;; slime
               (slime-repl-inputed-output-face :foreground ,fizzy-purple)
               ;; spam
               (spam :inherit gnus-summary-normal-read :foreground ,fizzy-orange
                     :strike-through t :slant oblique)
               ;; speedbar (and sr-speedbar)
               (speedbar-button-face :foreground ,fizzy-green)
               (speedbar-file-face :foreground ,fizzy-cyan)
               (speedbar-directory-face :foreground ,fizzy-purple)
               (speedbar-tag-face :foreground ,fizzy-yellow)
               (speedbar-selected-face :foreground ,fizzy-pink)
               (speedbar-highlight-face :inherit match)
               (speedbar-separator-face :background ,fizzy-bg
                                        :foreground ,fizzy-fg
                                        :weight bold)
               ;; tab-bar & tab-line (since Emacs 27.1)
               (tab-bar :foreground ,fizzy-purple :background ,fizzy-current
                        :inherit variable-pitch)
               (tab-bar-tab :foreground ,fizzy-pink :background ,fizzy-bg
                            :box (:line-width 2 :color ,fizzy-bg :style nil))
               (tab-bar-tab-inactive :foreground ,fizzy-purple :background ,bg2
                                     :box (:line-width 2 :color ,bg2 :style nil))
               (tab-line :foreground ,fizzy-purple :background ,fizzy-current
                         :height 0.9 :inherit variable-pitch)
               (tab-line-tab :foreground ,fizzy-pink :background ,fizzy-bg
                             :box (:line-width 2 :color ,fizzy-bg :style nil))
               (tab-line-tab-inactive :foreground ,fizzy-purple :background ,bg2
                                      :box (:line-width 2 :color ,bg2 :style nil))
               (tab-line-tab-current :inherit tab-line-tab)
               (tab-line-close-highlight :foreground ,fizzy-red)
               ;; term
               (term :foreground ,fizzy-fg :background ,fizzy-current)
               (term-color-black :foreground ,fizzy-bg :background ,fizzy-bg)
               (term-color-blue :foreground ,fizzy-purple :background ,fizzy-purple)
               (term-color-cyan :foreground ,fizzy-cyan :background ,fizzy-cyan)
               (term-color-green :foreground ,fizzy-green :background ,fizzy-green)
               (term-color-magenta :foreground ,fizzy-pink :background ,fizzy-pink)
               (term-color-red :foreground ,fizzy-red :background ,fizzy-red)
               (term-color-white :foreground ,fizzy-fg :background ,fizzy-fg)
               (term-color-yellow :foreground ,fizzy-yellow :background ,fizzy-yellow)
               ;; undo-tree
               (undo-tree-visualizer-current-face :foreground ,fizzy-orange)
               (undo-tree-visualizer-default-face :foreground ,fg2)
               (undo-tree-visualizer-register-face :foreground ,fizzy-purple)
               (undo-tree-visualizer-unmodified-face :foreground ,fizzy-fg)
               ;; web-mode
               (web-mode-builtin-face :inherit ,font-lock-builtin-face)
               (web-mode-comment-face :inherit ,font-lock-comment-face)
               (web-mode-constant-face :inherit ,font-lock-constant-face)
               (web-mode-doctype-face :inherit ,font-lock-comment-face)
               (web-mode-function-name-face :inherit ,font-lock-function-name-face)
               (web-mode-html-attr-name-face :foreground ,fizzy-purple)
               (web-mode-html-attr-value-face :foreground ,fizzy-green)
               (web-mode-html-tag-face :foreground ,fizzy-pink :weight bold)
               (web-mode-keyword-face :foreground ,fizzy-pink)
               (web-mode-string-face :foreground ,fizzy-yellow)
               (web-mode-type-face :inherit ,font-lock-type-face)
               (web-mode-warning-face :inherit ,font-lock-warning-face)
               ;; which-func
               (which-func :inherit ,font-lock-function-name-face)
               ;; whitespace
               (whitespace-big-indent :background ,fizzy-red :foreground ,fizzy-red)
               (whitespace-empty :background ,fizzy-orange :foreground ,fizzy-red)
               (whitespace-hspace :background ,bg3 :foreground ,fizzy-comment)
               (whitespace-indentation :background ,fizzy-orange :foreground ,fizzy-red)
               (whitespace-line :background ,fizzy-bg :foreground ,fizzy-pink)
               (whitespace-newline :foreground ,fizzy-comment)
               (whitespace-space :background ,fizzy-bg :foreground ,fizzy-comment)
               (whitespace-space-after-tab :background ,fizzy-orange :foreground ,fizzy-red)
               (whitespace-space-before-tab :background ,fizzy-orange :foreground ,fizzy-red)
               (whitespace-tab :background ,bg2 :foreground ,fizzy-comment)
               (whitespace-trailing :inherit trailing-whitespace)
               ;; yard-mode
               (yard-tag-face :inherit ,font-lock-builtin-face)
               (yard-directive-face :inherit ,font-lock-builtin-face))))

  (apply #'custom-theme-set-faces
         'fizzy
         (let ((color-names (mapcar #'car colors))
               (graphic-colors (mapcar #'cadr colors))
               (term-colors (mapcar #'car (mapcar #'cddr colors)))
               (tty-colors (mapcar #'car (mapcar #'last colors)))
               (expand-for-kind
                (lambda (kind spec)
                  (when (and (string= (symbol-name kind) "term-colors")
                             fizzy-use-24-bit-colors-on-256-colors-terms)
                    (setq kind 'graphic-colors))
                  (cl-progv color-names (symbol-value kind)
                    (eval `(backquote ,spec))))))
           (cl-loop for (face . spec) in faces
                    collect `(,face
                              ((((min-colors 16777216)) ; fully graphical envs
                                ,(funcall expand-for-kind 'graphic-colors spec))
                               (((min-colors 256))      ; terminal withs 256 colors
                                ,(funcall expand-for-kind 'term-colors spec))
                               (t                       ; should be only tty-like envs
                                ,(funcall expand-for-kind 'tty-colors spec))))))))


;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'fizzy)

;; Local Variables:
;; no-byte-compile: t
;; indent-tabs-mode: nil
;; End:

;;; fizzy-theme.el ends here
