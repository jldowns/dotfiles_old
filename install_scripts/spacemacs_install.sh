#!/bin/bash
# wget https://gitlab.com/jldowns/dotfiles/-/raw/master/install_scripts/spacemacs_install.sh

# Detect OS
source /etc/os-release

if [ "$(uname)" == "Darwin" ]; then
    echo "OSX not implemented yet"
elif [ "$ID" == "ubuntu" ]; then
    echo "Installing for ubuntu"
    add-apt-repository -y ppa:kelleyk/emacs
    apt update
    apt install -y git curl emacs27
    # install spacemacs
    git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
    # copy my .spacemacs
    curl https://gitlab.com/jldowns/dotfiles/-/raw/master/spacemacs/.spacemacs -o ~/.spacemacs
    # do the first run
    emacs --batch -l ~/.emacs.d/init.el --eval="(configuration-layer/update-packages t)"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    echo "windows not implemented yet."
fi

